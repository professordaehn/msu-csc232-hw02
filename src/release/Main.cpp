/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          <FILL ME IN ACCORDINGLY> 
 *          TODO: 1. (0.5 points) Replace the above line accordingly and
 *          commit with the commit message that follows. Before committing
 *          though, erase all these TODO instructions.
 *
 *          CSC232-HW02 - Updated authors.
 *          
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <iostream>

// TODO: 2. (1 point) Declare your Acker function prototype below:
//          When done, commit with the commit message that follows, being
//          sure to delete all these TODO instructions before actuallly
//          executing the git commit command.
//
//          CSC232-HW02 - Added Aker function prototype.
//
//          Don't forget to fully document the function prototype using
//          all the appropriate doxygen elements.

/**
 * @brief Entry point to this application.
 * @remark You are encouraged to modify this file as you see fit to gain
 * practice in using objects.
 *
 * @param argc the number of command line arguments
 * @param argv an array of the command line arguments
 * @return EXIT_SUCCESS upon successful completion
 */
int main(int argc, char **argv) {
    // TODO: 4. (0.5 points) Replace *** text *** with actual call to Acker
    //       function. When done, commit with the commit message that follows, 
    //       being sure to delete all these TODO instructions before actuallly
    //       executing the git commit command.
    //
    //       CSC232-HW02 - Display calculated Acker value.
    //
    std::cout << "Acker(1, 2) = " << "*** replace me with call to Acker***" 
              << std::endl;
    return EXIT_SUCCESS;
}

// TODO: 3. (2 points) Define the Acker recursive function below. When done, 
//       commit with the commit message that follows, being sure to delete
//       all these TODO instructions before actually executing the git
//       commit command.
//
//       CSC232-HW02 - Implemented Acker function.     

// TODO: 5. (1 point) Do a box trace of Acker(1, 2) using pen and paper. 
//       Hand this in at the beginning of class on Friday, 17 February 2017.
//       Your solution MUST be legible; no points are assigned to illegible 
//       papers.
